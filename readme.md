<h1 align="center">Welcome to NSO bank final project 👋</h1>
[![ts](https://badgen.net/badge/-/node?icon=typescript&label&labelColor=blue&color=555555)](https://github.com/TypeStrong/ts-node)
[<img src="https://img.shields.io/badge/-ESLint-4B32C3.svg?logo=ESlint">](https://eslint.org/)
[![Husky](https://img.shields.io/static/v1?label=Formated+by&message=Prettier&color=ff69b4)](https://github.com/prettier/prettier)
[![Husky](https://img.shields.io/static/v1?label=Hooked+by&message=Husky&color=success)](https://typicode.github.io/husky)
 ![ts](https://badgen.net/badge/-/TypeScript/blue?icon=typescript&label)
 [<img src="https://img.shields.io/badge/-SWC-FECC00.svg?logo=swc">](https://swc.rs/)


<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: RAPYD" src="https://img.shields.io/badge/License-RAPYD-yellow.svg" />
  </a>
</p>

> NSO payments platform 

### 🏦 [Homepage](https://bitbucket.org/shanisabag/final-project-ebank)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Routers

```sh
GET /api/account/individual/:account_id
POST /api/account/individual

GET /api/account/business/:account_id
POST /api/account/business

GET /api/account/family/:account_id
POST /api/account/family
POST /api/account/family/add_individuals_accounts/:account_id
POST /api/account/family/remove_individuals_accounts/:account_id
PATCH /api/account/family/close/:account_id

PATCH /api/account/status

GET /api/transfer/
GET /api/transfer/:account_id
POST /api/transfer/:business_src_id/trasfer_b2b/:business_dest_id
POST /api/transfer/:business_src_id/trasfer_b2i/:individual_dest_id
POST /api/transfer/:family_src_id/trasfer_f2b/:business_dest_id
POST /api/transfer/:business_src_id/trasfer_FX_b2b/:business_dest_id
POST /api/transfer/:individual_src_id/trasfer_I2F/:family_dest_id
```

## Request Signatures
### Calculation of Signature - Code Example

```sh
// access and secret key (generate from ahead to merchants).
 const apiKey = 'AAAAAAA';
 const apiSecret = 'aaaaaaa';
 // initialize hash string components.
 const timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();
 const url_path = 'urlPath';
 const salt = CryptoJS.lib.WordArray.random(12).toString();
 const http_method =pm.request.method.toLowerCase();
 var fullUrl = pm.request.url.toString()
 let body;
 if(pm.request.method !== "POST" || pm.request.method !== "PUT" || pm.request.method !== "PATCH"){
 body={}
} else{
body=JSON.parse(pm.request.body.raw); }
const to_sign = http_method +fullUrl + salt + timestamp + apiKey + apiSecret + body;
// generate signature/hash.
let signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, apiSecret));
// base 64 encoding of the signature.
signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));
// set headers for the request by the company api.
pm.request.headers.add({key: 'fullurl', value: fullUrl });
pm.request.headers.add({key: 'timestamp', value: timestamp });
pm.request.headers.add({key: 'signature', value:signature });
pm.request.headers.add({key: 'access_key', value:apiKey });
pm.request.headers.add({key: 'salt', value:salt });
```

## Author

👤 **Omri Azaria & Shani Sabag & Nadav Tal** 👤

## Show your support

Give a ❤️️ ifyou like this project!