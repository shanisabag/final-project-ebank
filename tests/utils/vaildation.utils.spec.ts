import { expect } from "chai";
import { onlyArrayOfNumberTuples, onlyLettersAndSpaces } from "../../src/utils/vaildation.utils";

describe("validation utils", () => {
    context("#onlyLettersAndSpaces", () => {
        it("should be a function", () => {
            expect(onlyLettersAndSpaces).to.be.a("function");
            expect(onlyLettersAndSpaces).to.be.instanceOf(Function);
        });

        it("should return true for input 'rapyd academy'", () => {
            expect(onlyLettersAndSpaces('rapyd academy')).to.eql(true);
        });

        it("should return false for input 'rapyd academy #2'", () => {
            expect(onlyLettersAndSpaces('rapyd academy #2')).to.eql(false);
        });
    });

    context("#onlyArrayOfNumberTuples", () => {
        it("should be a function", () => {
            expect(onlyArrayOfNumberTuples).to.be.a("function");
            expect(onlyArrayOfNumberTuples).to.be.instanceOf(Function);
        });

        it("should return true for input '[[1,200]]'", () => {
            expect(onlyArrayOfNumberTuples([[1,200]])).to.eql(true);
        });

        it("should return true for input '[[1,200],[2,300]]'", () => {
            expect(onlyArrayOfNumberTuples([[1,200],[2,300]])).to.eql(true);
        });

        it("should return true for input '[[1,'a'],[2,300]]'", () => {
            /* @ts-ignore */
            expect(onlyArrayOfNumberTuples([[1,'a'],[2,300]])).to.eql(false);
        });
    });
})



