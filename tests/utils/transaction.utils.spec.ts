import { expect } from "chai";
import trasaction_utils from "../../src/utils/transaction.utils";

describe("transaction utils", () => {
    context("#createTransactionObjectResult", () => {
        const payload = {
            src_id: 1234567,
            src_balance: 20000,
            src_currency: "USD",
            dest_id: 7654321,
            dest_balance: 40000,
            dest_currency: "USD"
        }

        const result = {
            src_account_id: 1234567,
            src_account_balance: 20000,
            src_account_currency: "USD",
            dest_account_id: 7654321,
            dest_account_balance: 40000,
            dest_account_currency: "USD",
        }

        it("should be a function", () => {
            expect(trasaction_utils.createTransactionObjectResult).to.be.a("function");
            expect(trasaction_utils.createTransactionObjectResult).to.be.instanceOf(Function);
        });

        it("a transaction object result", () => {
            expect(trasaction_utils.createTransactionObjectResult(payload.src_id, payload.src_balance, payload.src_currency, payload.dest_id, payload.dest_balance, payload.dest_currency)).to.deep.equal(result);
        });
    })
})