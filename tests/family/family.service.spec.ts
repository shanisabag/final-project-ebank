/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/unbound-method */
import { expect } from "chai";
import sinon from "sinon";
import family_service from "../../src/family/family.service";
import individual_service from "../../src/individual/individual.service";
import validation_controller from "../../src/validations/validation.controller";
import validation_service from "../../src/validations/validation.service";
import { accountsTypes, IFamily, IIndividual } from "../../src/types/types";
import account_service from "../../src/account/account.service";
import family_db_provider from "../../src/family/family.db.provider";
import config_data from "../../src/config.data";
import { FullFamilyResult, IResponseError, ShortFamilyResult } from "../../src/types/result.types";
import { RowDataPacket } from "mysql2";
describe("family service", () => {
    context("#getFamilyAccount", () => {
        let get_family_account: sinon.SinonStub<[account_id: number], Promise<RowDataPacket>>;
        before(() => {
            get_family_account = sinon.stub(family_db_provider, "getFamilyAccount");
        })
        after(() => {
            sinon.restore();
        })

        const payload = [
            {
                family_account_id: 3,
                family_account_currency: "USD",
                family_account_balance: 10000,
                family_account_is_active: true,
                family_account_type: "family" as accountsTypes,
                family_table_id: 3,
                family_table_context: "travel",
                account_id: 1,
                currency: "USD",
                balance: 10000,
                is_active: true,
                type: "individual" as accountsTypes,
                individual_table_id: 1,
                individual_id: 1234567,
                first_name: "Joe",
                last_name: "Lev",
                email: "joe@gamil.com", 
                address_id: 1,
                country_name: "Israel",
                country_code: 1,
                postal_code: 123456,
                city: "TLV",
                region: "",
                street_name: "Sokolov",
                street_number: 20          
            }, {
                family_account_id: 3,
                family_account_currency: "USD",
                family_account_balance: 10000,
                family_account_is_active: true,
                family_account_type: "family" as accountsTypes,
                family_table_id: 3,
                family_table_context: "travel",
                account_id: 2,
                currency: "USD",
                balance: 10000,
                is_active: true,
                type: "individual" as accountsTypes,
                individual_table_id: 2,
                individual_id: 7654321,
                first_name: "Jane",
                last_name: "Lev",
                email: "jane@gamil.com",  
                address_id: 1,
                country_name: "Israel",
                country_code: 1,
                postal_code: 123456,
                city: "TLV",
                region: "",
                street_name: "Sokolov",
                street_number: 20         
            }
        ]

        const short_result = {
            account_id: 3,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "family",
            family_table_id: 3,
            context: "travel",
            owners: [1, 2]
        }
    
        const full_result = {
            account_id: 3,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "family",
            family_table_id: 3,
            context: "travel",
            owners: [
                {
                    account_id: 1,
                    currency: "USD",
                    balance: 10000,
                    is_active: true,
                    type: "individual" as accountsTypes,
                    individual_table_id: 1,
                    individual_id: 1234567,
                    first_name: "Joe",
                    last_name: "Lev",
                    email: "joe@gamil.com",  
                    address: {
                        address_id: 1,
                        country_name: "Israel",
                        country_code: 1,
                        postal_code: 123456,
                        city: "TLV",
                        region: "",
                        street_name: "Sokolov",
                        street_number: 20         
                    }
                },
                {
                    account_id: 2,
                    currency: "USD",
                    balance: 10000,
                    is_active: true,
                    type: "individual" as accountsTypes,
                    individual_table_id: 2,
                    individual_id: 7654321,
                    first_name: "Jane",
                    last_name: "Lev",
                    email: "jane@gamil.com",  
                    address: {
                        address_id: 1,
                        country_name: "Israel",
                        country_code: 1,
                        postal_code: 123456,
                        city: "TLV",
                        region: "",
                        street_name: "Sokolov",
                        street_number: 20         
                    }
                }
            ]
        }

        it("should be a function", () => {
            expect(family_service.getFamilyAccount).to.be.a("function");
            expect(family_service.getFamilyAccount).to.be.instanceOf(Function);
        });

        it("succeeded - a short result", async () => {
            get_family_account.resolves(payload as RowDataPacket);
            expect(await family_service.getFamilyAccount(3)).to.deep.equal(short_result);
        });

        it("succeeded - a full result", async () => {
            get_family_account.resolves(payload as RowDataPacket);
            expect(await family_service.getFamilyAccount(3, "full")).to.deep.equal(full_result);
        });

        it("failed", async () => {
            get_family_account.resolves(undefined);
            try {
                await family_service.getFamilyAccount(3);
            } catch (error) {
               expect((error as IResponseError).message).to.equal("account 3 does not exsit."); 
            }
        });

    })
    
    context("#createFamilyAccount", () => {
        let get_individual_account: sinon.SinonStub<[account_id: number], Promise<IIndividual>>;
        let individual_to_family_validations: sinon.SinonStub<[src_account: IIndividual, amount: number, family_currency: string], boolean>;
        let validate_initial_amount: sinon.SinonStub<[initial_amount: number, account_type: string], boolean>;
        let create_account: sinon.SinonStub<[currency: string, balance: number, type: string], Promise<number>>;
        let create_family_account: sinon.SinonStub<[context: string, account_id: number], Promise<number>>;
        let add_individuals_accounts: sinon.SinonStub<any[], any>;
        
        before(() => {
            get_individual_account = sinon.stub(individual_service, "getIndividualAccount");
            individual_to_family_validations = sinon.stub(validation_controller, "individualToFamilyValidations");
            validate_initial_amount = sinon.stub(validation_service, "validateInitialAmount");
            create_account = sinon.stub(account_service, "createAccount");
            create_family_account = sinon.stub(family_db_provider, "createFamilyAccount");
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            add_individuals_accounts = sinon.stub(family_service, "addIndividualsAccounts" as any).callsFake(async () => {});
        })

        after(() => {
            sinon.restore();
        })

        const individual_account = {
            account_id: 1,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "individual" as accountsTypes,
            individual_table_id: 1234567,
            individual_id: 1234567,
            first_name: "Joe",
            last_name: "Lev",
            email: "joe@gamil.com",
            address: {
                address_id: 1,
                country_name: "Israel",
                country_code: 1,
                postal_code: 123456,
                city: "TLV",
                region: "",
                street_name: "Sokolov",
                street_number: 20
            }              
        }

        const valid_payload = {
            context: "travel",
            owners: [[1,5000]],
            currency: "USD"
        }

        const invalid_payload = {
            context: "travel",
            owners: [[1,-5000]],
            currency: "USD"
        }

        it("should be a function", () => {
            expect(family_service.createFamilyAccount).to.be.a("function");
            expect(family_service.createFamilyAccount).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            get_individual_account.resolves(individual_account);
            individual_to_family_validations.returns(true);
            validate_initial_amount.returns(true);
            create_account.resolves(2);
            create_family_account.resolves(3);
            add_individuals_accounts;
            expect(await family_service.createFamilyAccount(valid_payload as Partial<IFamily>)).to.equal(2);
            sinon.assert.calledOnce(get_individual_account);
            sinon.assert.calledOnce(individual_to_family_validations);
            sinon.assert.calledOnce(validate_initial_amount);
            sinon.assert.calledOnce(create_account);
            sinon.assert.calledOnce(create_family_account);
            sinon.assert.calledOnce(add_individuals_accounts);
        });

        it("failed", async () => {
            get_individual_account.resolves(individual_account);
            individual_to_family_validations.returns(true);
            validate_initial_amount.returns(false);
            create_account.resolves(2);
            create_family_account.resolves(3);
            add_individuals_accounts;  
            try {
                await family_service.createFamilyAccount(invalid_payload as Partial<IFamily>);
            } catch (error) {
               expect((error as IResponseError).message).to.equal(`all of the owners initial contribution amount should sum up to a min of ${config_data.minimum_initial_amount.family}`); 
            }
        });
    })

    context("#validateAndRemoveIndividualsAccounts", () => {
        let get_family_account: sinon.SinonStub<[account_id: number, details_level?: string | undefined], Promise<ShortFamilyResult | FullFamilyResult>>;
        let get_individual_account: sinon.SinonStub<[account_id: number], Promise<IIndividual>>;
        let is_belong_to_family: sinon.SinonStub<[individual_id: number, family_id: number], Promise<boolean>>;
        let validate_family_balance_after_removes: sinon.SinonStub<[family_id: number, family_balance: number, total_amount: number, total_accounts_to_remove: number], Promise<boolean>>;
        let remove_individuals_accounts: sinon.SinonStub<any[], any>;

        before(() => {
            get_family_account = sinon.stub(family_service, "getFamilyAccount");
            get_individual_account = sinon.stub(individual_service, "getIndividualAccount");
            is_belong_to_family = sinon.stub(family_db_provider, "isBelongToFamily");
            validate_family_balance_after_removes = sinon.stub(family_service, "validateFamilyBalanceAfterRemoves");
            remove_individuals_accounts = sinon.stub(family_service, "removeIndividualsAccounts" as any).callsFake(async () => {});
        })

        after(() => {
            sinon.restore();
        })
        const individual_account = {
            account_id: 1,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "individual" as accountsTypes,
            individual_table_id: 1234567,
            individual_id: 1234567,
            first_name: "Joe",
            last_name: "Lev",
            email: "joe@gamil.com",
            address: {
                address_id: 1,
                country_name: "Israel",
                country_code: 1,
                postal_code: 123456,
                city: "TLV",
                region: "",
                street_name: "Sokolov",
                street_number: 20
            }              
        }

        const family_account = {
            account_id: 3,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "family",
            family_table_id: 3,
            context: "travel",
            owners: [1, 2]
        }

        const valid_owners = {
            owners: [[1,5000]] as [number, number][],
        }

        const invalid_owners = {
            owners: [[1,-5000]] as [number, number][],
        }

        it("should be a function", () => {
            expect(family_service.validateAndRemoveIndividualsAccounts).to.be.a("function");
            expect(family_service.validateAndRemoveIndividualsAccounts).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            get_family_account.resolves(family_account);
            get_individual_account.resolves(individual_account);
            is_belong_to_family.resolves(true);
            validate_family_balance_after_removes.resolves(true);
            remove_individuals_accounts;
            expect(await family_service.validateAndRemoveIndividualsAccounts(valid_owners.owners , 3)).to.eql(true);
            sinon.assert.calledOnce(get_individual_account);
            sinon.assert.calledOnce(is_belong_to_family);
            sinon.assert.calledOnce(validate_family_balance_after_removes);
            sinon.assert.calledOnce(remove_individuals_accounts);
        });

        it("failed - individual account has negative amount", async () => {
            get_family_account.resolves(family_account);
            get_individual_account.resolves(individual_account);
            is_belong_to_family.resolves(true);
            validate_family_balance_after_removes.resolves(true);
            remove_individuals_accounts; 
            try {
                await family_service.validateAndRemoveIndividualsAccounts(invalid_owners.owners, 3);
            } catch (error) {
               expect((error as IResponseError).message).to.equal(`account 1 has negative amount`); 
            }
        });

        it("failed - individual account does not belong to family account", async () => {
            get_family_account.resolves(family_account);
            get_individual_account.resolves(individual_account);
            is_belong_to_family.resolves(true);
            validate_family_balance_after_removes.resolves(true);
            remove_individuals_accounts; 
            try {
                await family_service.validateAndRemoveIndividualsAccounts(valid_owners.owners, 3);
            } catch (error) {
               expect((error as IResponseError).message).to.equal(`account 1 does not belong to family account`); 
            }
        });
    })

    context("#validateFamilyBalanceAfterRemoves", () => {
        let total_owners : sinon.SinonStub<[family_id: number], Promise<number>>;
        let validate_initial_amount : sinon.SinonStub<[initial_amount:number, account_type:string], boolean>;

        before(() => {
            total_owners = sinon.stub(family_db_provider, "countFamilyOwners");
            validate_initial_amount = sinon.stub(validation_service, "validateInitialAmount");
        })

        after(() => {
            sinon.restore();
        })

        it("should be a function", () => {
            expect(family_service.validateFamilyBalanceAfterRemoves).to.be.a("function");
            expect(family_service.validateFamilyBalanceAfterRemoves).to.be.instanceOf(Function);
        });

        it("succeeded - removing all of the family owners + total_amount < family_balance", async () => {
            total_owners.resolves(5);
            expect(await family_service.validateFamilyBalanceAfterRemoves(1, 10000, 500, 5)).to.eql(true);
            sinon.assert.notCalled(validate_initial_amount);
        });

        it("failed - removing all of the family owners + total_amount > family_balance", async () => {
            total_owners.resolves(5);
            try {
                await family_service.validateFamilyBalanceAfterRemoves(1, 10000, 50000, 5)
            } catch (error) {
                expect((error as IResponseError).message).to.equal("the total amount can not be greater than the family account's balance"); 
             }
        });

       
    })
})