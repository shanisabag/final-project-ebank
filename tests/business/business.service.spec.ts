/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/unbound-method */
import { expect } from "chai";
import sinon from "sinon";
import address_service from "../../src/address/address.service.js";
import {IAccount, IAddress, IBusiness } from "../../src/types/types";
import account_service from "../../src/account/account.service";
import business_db_provider from "../../src/business/business.db.provider.js";
import business_service from "../../src/business/business.service.js";
import {IResponseError} from "../../src/types/result.types.js"

describe("Business service", () => {
    context("#getBusinessAccount", () => {
        let get_business: sinon.SinonStub<[account_id: number], Promise<IBusiness>>;

        before(() => {
            get_business = sinon.stub(business_db_provider, "getBusinessAccount");
        })

        after(() => {
            sinon.restore();
        })

        const business_account ={
            company_id: 1,
            company_name: "NSO",
            context: "FUN",
            currency: "NIS",
            balance: 400,      
        }

        it("should be a function", () => {
            expect(business_service.getBusinessAccount).to.be.a("function");
            expect(business_service.getBusinessAccount).to.be.instanceOf(Function);
        });

        it("succeeded 1", async () => {
            get_business.resolves(business_account as IBusiness);
            expect(await business_service.getBusinessAccount(1)).to.equal(business_account);
            sinon.assert.calledOnce(get_business);
        });

        it("succeeded 2", async () => {
            expect(await business_service.getBusinessAccount(1)).to.equal(business_account);
            sinon.assert.calledWith(get_business, 1);
        });

        it("failed", async () => {
            get_business.resolves("" as unknown as undefined);
            try {
                await business_service.getBusinessAccount(1);
            } catch (error) {
            expect((error as IResponseError).message).to.equal(`account 1 does not exsit`); 
            }
        });

    })

    context("#createBusinessAccount", () => {
        let create_account: sinon.SinonStub<[currency: string, balance: number, type: string], Promise<number>>;
        let create_address: sinon.SinonStub<[payload: IAddress], Promise<number>>;
        let create_business: sinon.SinonStub<[company_id: number, company_name: string, context: string, account_id: number, address_id: number], Promise<number>>;
        
        before(() => {
            create_account = sinon.stub(account_service, "createAccount");
            create_address = sinon.stub(address_service, "createAddress");
            create_business = sinon.stub(business_db_provider, "createBusinessAccount");
        })

        after(() => {
            sinon.restore();
        })
        
        const business_account ={
            company_id: 99999999,
            company_name: "NSO",
            context: "FUN",
            currency: "NIS",
            balance: 400,      
        }

        it("should be a function", () => {
            expect(business_service.createBusinessAccount).to.be.a("function");
            expect(business_service.createBusinessAccount).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            create_account.resolves(5);
            create_business.resolves(5);
            expect(await business_service.createBusinessAccount(business_account as Partial<IAccount>)).to.equal(5);
            sinon.assert.notCalled(create_address);
        });
    })
})
