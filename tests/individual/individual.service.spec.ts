/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/unbound-method */
import { expect } from "chai";
import sinon from "sinon";
import individual_service from "../../src/individual/individual.service";
import address_service from "../../src/address/address.service.js";
import {IAccount,IAddress,IIndividual } from "../../src/types/types";
import account_service from "../../src/account/account.service";
import individual_db_provider from "../../src/individual/individual.db.provider.js";
import {IResponseError} from "../../src/types/result.types.js"

describe("individual service", () => {
    context("#getIndividualAccount", () => {
        let get_individual: sinon.SinonStub<[account_id: number], Promise<IIndividual>>;
        
        before(() => {
            get_individual = sinon.stub(individual_db_provider, "getIndividualAccount");
        })

        after(() => {
            sinon.restore();
        })

        const individual ={
            individual_id: 1,
            first_name: "jon",
            last_name: "snow",
            email: "jon@snow.com",
            address: "somwhere"
        }

        it("should be a function", () => {
            expect(individual_service.getIndividualAccount).to.be.a("function");
            expect(individual_service.getIndividualAccount).to.be.instanceOf(Function);
        });

        it("succeeded 1", async () => {
            get_individual.resolves(individual as unknown as IIndividual);
            expect(await individual_service.getIndividualAccount(1)).to.equal(individual);
            sinon.assert.calledOnce(get_individual);
        });

        it("succeeded 2", async () => {
            expect(await individual_service.getIndividualAccount(1)).to.equal(individual);
            sinon.assert.calledWith(get_individual, 1);
        });

        it("failed", async () => {
            get_individual.resolves("" as unknown as undefined);
            try {
                await individual_service.getIndividualAccount(1);
            } catch (error) {
                expect((error as IResponseError).message).to.equal(`account 1 does not exsit.`); 
            }
        });
    })

    context("#createIndividualAccount", () => {
        let get_individual_account: sinon.SinonStub<[individual_id: number], Promise<IIndividual>>;
        let create_account: sinon.SinonStub<[currency: string, balance: number, type: string], Promise<number>>;
        let create_address: sinon.SinonStub<[payload: IAddress], Promise<number>>;
        let create_individual: sinon.SinonStub<[individual_id: number, first_name: string, last_name: string, email: string, account_id: number, address_id: number], Promise<number>>;
        
        before(() => {
            get_individual_account = sinon.stub(individual_db_provider, "getIndividualById");
            create_account = sinon.stub(account_service, "createAccount");
            create_address = sinon.stub(address_service, "createAddress");
            create_individual = sinon.stub(individual_db_provider, "createIndividualAccount");
        })

        after(() => {
            sinon.restore();
        })

        const individual ={
            individual_id: 204223445,
            first_name: "jon",
            last_name: "snow",
            email: "jon@snow.com",
            address: "somwhere"
        }

        it("should be a function", () => {
            expect(individual_service.createIndividualAccount).to.be.a("function");
            expect(individual_service.createIndividualAccount).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            get_individual_account.resolves(false as unknown as undefined);
            create_account.resolves(2);
            create_address.resolves(2);
            create_individual.resolves(2);
            expect(await individual_service.createIndividualAccount(individual as Partial<IAccount>)).to.equal(2);
            sinon.assert.calledOnce(create_address);
        });

        it("failed", async () => {
            get_individual_account.resolves(true as unknown as IIndividual);
            try {
                await individual_service.createIndividualAccount(individual as Partial<IAccount>);
            } catch (error) {
               expect((error as IResponseError).message).to.equal(`individual account already exists`); 
            }
        });
    })
})
