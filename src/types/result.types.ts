import { accountsTypes, IIndividual } from "./types.js";

export interface TransactionResult {
    src_account_id: number;
    src_account_balance: number;
    src_account_currency: string;
    dest_account_id: number;
    dest_account_balance: number;
    dest_account_currency: string;
}

export interface FamilyResult {
    account_id: number;
    currency: string;
    balance: number;
    is_active: boolean;
    type: string;
    family_table_id: number;
    context: string;
}

export interface ShortFamilyResult extends FamilyResult {
    owners: number[];
}

export interface FullFamilyResult extends FamilyResult {
    owners: IIndividual[];
}

export interface AccountsStatusResponse {
    accounts: [number, accountsTypes][];
    status: string;
}

export interface IResponseError {
    status: number;
    message: string;
    stack: string;
}

export interface IFamilyResultRowDataPacket {
    [key: string]: any;
}

export interface IGetRateResult {
    success: boolean;
    timestamp: number;
    base: string;
    date: string;
    rates: IRates;
}

interface IRates {
    [key: string]: number;
}
