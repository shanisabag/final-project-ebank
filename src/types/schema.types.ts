export interface ISchema {
    [key: string]: Function[];
}

export interface IPayload {
    [key: string]: any;
}
