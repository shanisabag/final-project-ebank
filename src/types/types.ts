export type accountsTypes = "business" | "individual" | "family";
export type accounts = "business" | "individual" | "family" | "partners";

export interface IAddress {
    address_id: number;
    country_name: string;
    country_code: number;
    postal_code: number;
    city: string;
    region: string;
    street_name: string;
    street_number: number;
}

export interface IAccount {
    account_id: number;
    currency: string;
    balance: number;
    is_active: boolean;
    type: accountsTypes;
}

export interface IIndividual extends IAccount {
    individual_table_id: number;
    individual_id: number;
    first_name: string;
    last_name: string;
    email: string;
    address: IAddress;
}

export interface IBusiness extends IAccount {
    business_table_id: number;
    company_id: number;
    company_name: string;
    context: "payroll" | "treasury" | "investments";
    address: IAddress;
}

export interface IFamily extends IAccount {
    family_table_id: number;
    context: "travel" | "morgage" | "emergency" | "savings" | "checking";
    owners: [number, number][];
}

export interface IAuth {
    access_key: string;
    secret_key: string;
    roles: string;
}
declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Express {
        interface Request {
            id: string;
            account: IAccount;
            src: IAccount;
            dst: IAccount;
            amount: number;
            roles: string[];
        }
    }
}
