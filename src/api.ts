import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { setId } from "./middleware/id.middleware.js";
import { logger } from "./middleware/logger.middleware.js";
import {
    loggerError,
    responseWithError,
    urlNotFound,
} from "./middleware/errors.handler.js";
import connectDB from "./db/mysql.connection.js";
import individual_router from "./individual/individual.router.js";
import business_router from "./business/business.router.js";
import family_router from "./family/family.router.js";
import account_router from "./account/account.router.js";
import transaction_router from "./transaction/transaction.router.js";
import config_data from "./config.data.js";
import { setRoles, vertifyAuth } from "./auth/auth.middleware.js";
import raw from "./middleware/route.async.wrapper.js";
class Api {
    private _app;

    constructor(private _HOST: string, private _PORT: number) {
        this._app = express();
    }

    init() {
        // middleware
        this._app.use(cors());
        this._app.use(morgan("dev"));
        this._app.use(express.json());
        this._app.use(setId);
        this._app.use(logger(config_data.log_file_path));
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        this._app.use(raw(vertifyAuth));
        this._app.use(setRoles);

        // routing
        this.routing();

        // error handling
        this.errorHandler();

        // start the express api server
        this.connectToDB().catch(console.log);
    }

    private routing() {
        this._app.use("/api/account/individual", individual_router);
        this._app.use("/api/account/business", business_router);
        this._app.use("/api/account/family", family_router);
        this._app.use("/api/account/", account_router);
        this._app.use("/api/transfer/", transaction_router);
    }

    private errorHandler() {
        this._app.use("*", urlNotFound);
        this._app.use(loggerError(config_data.error_log_file_path));
        this._app.use(responseWithError);
    }

    private async connectToDB() {
        // connect to db
        await connectDB(
            config_data.sql.db_host,
            config_data.sql.db_port,
            config_data.sql.db_name,
            config_data.sql.db_user_name,
            config_data.sql.db_user_password
        );
        this._app.listen(this._PORT, this._HOST);
        log.green(
            `💰💰 NSO api is live on http://${this._HOST}:${this._PORT} 💰💰`
        );
    }
}

const app = new Api(config_data.host, config_data.port);
app.init();
