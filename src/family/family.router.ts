/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
import { vertifySanctionsList } from "../auth/auth.middleware.js";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import family_controller from "./family.controller.js";
import {
    family_schema,
    individuals_accounts_payload,
} from "./family.schema.js";

const router = express.Router();

// get a family account
router.get("/:account_id", raw(family_controller.getFamilyAccount));

// verify sanctions list
router.use(raw(vertifySanctionsList));

// create a family account
router.post(
    "/",
    shape(family_schema),
    raw(family_controller.createFamilyAccount)
);

// add individuals accounts to family account
router.post(
    "/add_individuals_accounts/:account_id",
    shape(individuals_accounts_payload),
    raw(family_controller.addIndividualsAccounts)
);

// remove individual from family account
router.post(
    "/remove_individuals_accounts/:account_id",
    shape(individuals_accounts_payload),
    raw(family_controller.removeIndividualsAccounts)
);

// close family account
router.patch("/close/:account_id", raw(family_controller.closeFamilyAccount));

export default router;
