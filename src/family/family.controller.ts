import { Request, Response } from "express";
import family_service from "./family.service.js";
import { IFamily } from "../types/types.js";

class FamilyController {
    async getFamilyAccount(req: Request, res: Response): Promise<void> {
        const account_id = Number(req.params.account_id);
        const family_account = await family_service.getFamilyAccount(
            account_id,
            req.query.details_level as string
        );
        res.status(200).json(family_account);
    }

    async createFamilyAccount(req: Request, res: Response): Promise<void> {
        const account_id = await family_service.createFamilyAccount(
            req.body as Partial<IFamily>
        );
        const family_account = await family_service.getFamilyAccount(
            account_id
        );
        res.status(200).json(family_account);
    }

    async addIndividualsAccounts(req: Request, res: Response): Promise<void> {
        const account_id = Number(req.params.account_id);
        const { owners } = req.body;

        await family_service.validateAndAddIndividualsAccounts(
            owners as [number, number][],
            account_id
        );
        const updated_family_account = await family_service.getFamilyAccount(
            account_id,
            req.query.details_level as string
        );
        res.status(200).json(updated_family_account);
    }

    async removeIndividualsAccounts(
        req: Request,
        res: Response
    ): Promise<void> {
        const account_id = Number(req.params.account_id);
        const { owners } = req.body;

        await family_service.validateAndRemoveIndividualsAccounts(
            owners as [number, number][],
            account_id
        );
        const updated_family_account = await family_service.getFamilyAccount(
            account_id,
            req.query.details_level as string
        );
        res.status(200).json(updated_family_account);
    }

    async closeFamilyAccount(req: Request, res: Response): Promise<void> {
        const account_id = Number(req.params.account_id);

        // close family - is_active = 0
        await family_service.closeFamilyAccount(account_id);
        res.status(200).json(
            `family account ${account_id} successfully closed.`
        );
    }
}

const family_controller = new FamilyController();
export default family_controller;
