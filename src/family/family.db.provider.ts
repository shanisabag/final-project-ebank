/* eslint-disable @typescript-eslint/no-unused-vars */
import { ResultSetHeader, RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";

class FamilyDBProvider {
    async getFamilyAccount(account_id: number): Promise<RowDataPacket> {
        const sql_select = `select af.account_id as family_account_id, af.currency as family_account_currency, af.balance as family_account_balance, af.is_active as family_account_is_active, af.type as family_account_type,
        fa.family_table_id as family_table_id, fa.context as family_table_context, ia.*, ad.*, ai.*
        from account af
        INNER JOIN family_account fa ON af.account_id = fa.account_id 
        LEFT JOIN individual_family_account ifa ON fa.family_table_id = ifa.family_id 
        LEFT JOIN individual_account ia ON ifa.individual_id = ia.individual_table_id
        LEFT JOIN address ad ON ia.address_id = ad.address_id
        LEFT JOIN account ai ON ia.account_id = ai.account_id
        WHERE af.account_id = ${account_id}`;
        const [query_result] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return query_result;
    }

    async createFamilyAccount(context: string, account_id: number) {
        const sql_insert = `INSERT INTO family_account (context, account_id) VALUES ("${context}", ${account_id})`;
        const [account] = (await connection.query(
            sql_insert
        )) as ResultSetHeader[];
        return account.insertId;
    }

    async addIndividualAccount(individual_id: number, family_id: number) {
        const sql_insert = "INSERT INTO individual_family_account SET ?";
        const [result] = await connection.query(sql_insert, {
            individual_id,
            family_id,
        });
    }

    async removeIndividualAccount(individual_id: number, family_id: number) {
        const sql_delete = `DELETE FROM individual_family_account WHERE individual_id = ${individual_id} AND family_id = ${family_id}`;
        const [result] = await connection.query(sql_delete);
    }

    async isBelongToFamily(individual_id: number, family_id: number) {
        const sql_select = `SELECT * FROM individual_family_account WHERE individual_family_account.individual_id = ${individual_id} AND individual_family_account.family_id = ${family_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        if (account[0]) {
            return true;
        }
        return false;
    }

    async countFamilyOwners(family_id: number) {
        const sql_select = `SELECT COUNT(*) AS count FROM individual_family_account WHERE individual_family_account.family_id = ${family_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        /* eslint-disable @typescript-eslint/no-unsafe-member-access */
        /* eslint-disable @typescript-eslint/no-unsafe-return */
        return account[0].count;
    }
}

const family_db_provider = new FamilyDBProvider();
export default family_db_provider;
