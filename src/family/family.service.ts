import account_db_provider from "../account/account.db.provider.js";
import account_service from "../account/account.service.js";
import config_data from "../config.data.js";
import { connection } from "../db/mysql.connection.js";
import HttpException from "../exceptions/http.exception.js";
import individual_service from "../individual/individual.service.js";
import transaction_service from "../transaction/transaction.service.js";
import { FullFamilyResult, ShortFamilyResult } from "../types/result.types.js";
import { IFamily } from "../types/types.js";
import {
    createFullFamilyResult,
    createShortFamilyResult,
} from "../utils/family.utils.js";
import validation_controller from "../validations/validation.controller.js";
import validation_service from "../validations/validation.service.js";
import family_db_provider from "./family.db.provider.js";

class FamilyService {
    async getFamilyAccount(
        account_id: number,
        details_level: string = "short"
    ): Promise<ShortFamilyResult | FullFamilyResult> {
        let family_account;
        const account = await family_db_provider.getFamilyAccount(account_id);
        if (!account) {
            throw new HttpException(
                400,
                `account ${account_id} does not exsit.`
            );
        }

        if (details_level === "full") {
            family_account = createFullFamilyResult(account);
        } else {
            family_account = createShortFamilyResult(account);
        }
        return family_account;
    }

    async createFamilyAccount(payload: Partial<IFamily>): Promise<number> {
        let total_amount = 0;

        const { context, currency, owners } = payload;

        // validate individuals accounts
        for (const [account_id, amount] of owners as [number, number][]) {
            const individual_account =
                await individual_service.getIndividualAccount(account_id);
            validation_controller.individualToFamilyValidations(
                individual_account,
                amount,
                currency as string
            );
            total_amount += amount;
        }

        // check if all of the owners initial contribution amount sum up to a min of 5,000 units of currency
        const valid_init_amount = validation_service.validateInitialAmount(
            total_amount,
            config_data.accounts_types.family
        );
        if (!valid_init_amount) {
            throw new HttpException(
                400,
                `all of the owners initial contribution amount should sum up to a min of ${config_data.minimum_initial_amount.family}`
            );
        }

        const account_id = await account_service.createAccount(
            currency as string,
            0,
            config_data.accounts_types.family
        );
        const family_id = await family_db_provider.createFamilyAccount(
            context as string,
            account_id
        );

        await this.addIndividualsAccounts(
            owners as [number, number][],
            account_id,
            family_id
        );
        return account_id;
    }

    async validateAndAddIndividualsAccounts(
        individuals: [number, number][],
        family_account_id: number
    ) {
        const family_account = await this.getFamilyAccount(family_account_id);

        // validate individuals accounts
        for (const [account_id, amount] of individuals) {
            const individual_account =
                await individual_service.getIndividualAccount(account_id);

            // check if individual already in family
            const is_belong = await family_db_provider.isBelongToFamily(
                individual_account.individual_table_id,
                family_account.family_table_id
            );
            if (is_belong) {
                throw new HttpException(
                    400,
                    `account ${account_id} already in family account`
                );
            }

            validation_controller.individualToFamilyValidations(
                individual_account,
                amount,
                family_account.currency
            );
        }

        // add individuals accounts
        await this.addIndividualsAccounts(
            individuals,
            family_account_id,
            family_account.family_table_id
        );
        return true;
    }

    async validateAndRemoveIndividualsAccounts(
        individuals: [number, number][],
        family_account_id: number
    ) {
        let total_amount = 0;
        const family_account = await this.getFamilyAccount(family_account_id);

        // validate individuals accounts to remove
        for (const [account_id, amount] of individuals) {
            const individual_account =
                await individual_service.getIndividualAccount(account_id);

            // check if amount is positive
            if (amount < 0) {
                throw new HttpException(
                    400,
                    `account ${account_id} has negative amount`
                );
            }

            // check if account belongs to family
            const is_belong = await family_db_provider.isBelongToFamily(
                individual_account.individual_table_id,
                family_account.family_table_id
            );
            if (!is_belong) {
                throw new HttpException(
                    400,
                    `account ${account_id} does not belong to family account`
                );
            }
            total_amount += amount;
        }

        // validate family account's balance after removing owners
        await this.validateFamilyBalanceAfterRemoves(
            family_account.family_table_id,
            family_account.balance,
            total_amount,
            individuals.length
        );

        // remove individuals accounts
        await this.removeIndividualsAccounts(
            individuals,
            family_account_id,
            family_account.family_table_id
        );
        return true;
    }

    async closeFamilyAccount(account_id: number) {
        const family_account = await family_service.getFamilyAccount(
            account_id
        );

        // check if family account have no assigned individual accounts
        await this.validateFamilyAccountToClose(family_account.family_table_id);

        const result = await account_db_provider.updateAccountActivation(
            account_id,
            false
        );
        if (!result) {
            throw new HttpException(
                400,
                `can not close family account ${account_id}`
            );
        }
    }

    private async addIndividualsAccounts(
        individuals: [number, number][],
        family_account_id: number,
        family_id: number
    ) {
        await connection.beginTransaction();
        try {
            for (const [account_id, amount] of individuals) {
                // add to individual_family_account table
                const individual_account =
                    await individual_service.getIndividualAccount(account_id);
                await family_db_provider.addIndividualAccount(
                    individual_account.individual_table_id,
                    family_id
                );

                // transfer from individual account to family account
                await transaction_service.createTransfer(
                    individual_account.account_id,
                    config_data.accounts_types.individual,
                    family_account_id,
                    config_data.accounts_types.family,
                    amount,
                    config_data.limit_flag.I2F,
                    false
                );
            }
            await connection.commit();
        } catch (error) {
            await connection.rollback();
            throw error;
        }
    }

    private async removeIndividualsAccounts(
        individuals: [number, number][],
        family_account_id: number,
        family_id: number
    ) {
        await connection.beginTransaction();
        try {
            for (const [account_id, amount] of individuals) {
                const individual_account =
                    await individual_service.getIndividualAccount(account_id);

                // remove from individual_family_account table
                await family_db_provider.removeIndividualAccount(
                    individual_account.individual_table_id,
                    family_id
                );

                // transfer from family account to individual account
                await transaction_service.createTransfer(
                    family_account_id,
                    config_data.accounts_types.family,
                    individual_account.account_id,
                    config_data.accounts_types.individual,
                    amount,
                    config_data.limit_flag.F2I,
                    false
                );
            }
            await connection.commit();
        } catch (error) {
            await connection.rollback();
            throw error;
        }
    }

    async validateFamilyBalanceAfterRemoves(
        family_id: number,
        family_balance: number,
        total_amount: number,
        total_accounts_to_remove: number
    ) {
        const total_owners = await family_db_provider.countFamilyOwners(
            family_id
        );
        // in case of removing all of the family owners, the total amounts of the removed
        // owner can result in up to a zero amount, but not go below 0.
        if (total_owners === total_accounts_to_remove) {
            if (total_amount > family_balance) {
                throw new HttpException(
                    400,
                    "the total amount can not be greater than the family account's balance"
                );
            }
            return true;
        }

        // ensure a minimum allowed balance is being kept after the removal
        const valid_init_amount = validation_service.validateInitialAmount(
            family_balance - total_amount,
            config_data.accounts_types.family
        );
        if (!valid_init_amount) {
            throw new HttpException(
                400,
                `a min balance of ${config_data.minimum_initial_amount.family} should be kept after removal`
            );
        }

        return true;
    }

    async validateFamilyAccountToClose(family_id: number) {
        const total_owners = await family_db_provider.countFamilyOwners(
            family_id
        );
        if (total_owners !== 0) {
            throw new HttpException(
                400,
                "family account should have no assigned individual accounts"
            );
        }
        return true;
    }

    async isBelongToFamily(
        individual_account_id: number,
        family_account_id: number
    ) {
        const individual_account =
            await individual_service.getIndividualAccount(
                individual_account_id
            );
        const family_account = await family_service.getFamilyAccount(
            family_account_id
        );

        const is_belong = await family_db_provider.isBelongToFamily(
            individual_account.individual_table_id,
            family_account.family_table_id
        );
        if (!is_belong) {
            throw new HttpException(
                400,
                `account ${individual_account_id} is not belong to the family`
            );
        }
        return true;
    }
}

const family_service = new FamilyService();
export default family_service;
