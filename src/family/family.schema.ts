import config_data from "../config.data.js";
import validation_schema from "../validations/validation.schema.js";

export const family_schema = {
    context: [validation_schema.string()],
    owners: [
        validation_schema.require(),
        validation_schema.arrayOfNumberTuples(),
    ],
    currency: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.currencies(config_data.supported_currencies),
    ],
};

export const individuals_accounts_payload = {
    owners: [
        validation_schema.require(),
        validation_schema.arrayOfNumberTuples(),
    ],
};
