/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { NextFunction, Request, Response } from "express";
import config_data from "../config.data.js";
import HttpException from "../exceptions/http.exception.js";
import { IPayload, ISchema } from "../types/schema.types.js";

class ValidationSchema {
    require(err_msg?: string) {
        return function (value: any, field: string) {
            if (value) {
                return true;
            }
            const msg = err_msg ? err_msg : `${field} is a required field`;
            throw new HttpException(400, msg);
        };
    }

    number(err_msg?: string) {
        return function (value: any, field: string) {
            if (!value || typeof value === "number") {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not a number type`;
            throw new HttpException(400, msg);
        };
    }

    oneOf(values: any[], err_msg?: string) {
        return function (value: any, field: string) {
            if (values.includes(value)) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not one of the valid values`;
            throw new HttpException(400, msg);
        };
    }

    string(err_msg?: string) {
        return function (value: any, field: string) {
            if (!value || typeof value === "string") {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not a string type`;
            throw new HttpException(400, msg);
        };
    }

    mail(err_msg?: string) {
        return function (value: string, field: string) {
            const mail_regex =
                /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (!value || mail_regex.exec(value)) {
                return true;
            }
            const msg = err_msg ? err_msg : `${field}:${value} is not a mail`;
            throw new HttpException(400, msg);
        };
    }

    min(min_value: number, err_msg?: string) {
        return function (value: number, field: string) {
            if (!value || value >= min_value) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field} the minimun value is ${min_value}`;
            throw new HttpException(400, msg);
        };
    }

    max(max_value: number, err_msg?: string) {
        return function (value: number, field: string) {
            if (!value || value <= max_value) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field} the maximum value is ${max_value}`;
            throw new HttpException(400, msg);
        };
    }

    minLength(min_value: number, err_msg?: string) {
        return function (value: string, field: string) {
            if (!value || value.length >= min_value) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field} the minimun number of string characters is ${min_value}`;
            throw new HttpException(400, msg);
        };
    }

    maxLength(max_value: number, err_msg?: string) {
        return function (value: string, field: string) {
            if (!value || value.toString().length <= max_value) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field} the maximum number of string characters is ${max_value}`;
            throw new HttpException(400, msg);
        };
    }

    currencies(currencies: string[], err_msg?: string) {
        return function (value: string, field: string) {
            if (!value || currencies.includes(value)) {
                return true;
            }
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not supported`;
            throw new HttpException(400, msg);
        };
    }

    arrayOfNumberTuples(err_msg?: string) {
        return function (value: string, field: string) {
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not array of tuples [ number, number ] with at least one element`;
            if (!Array.isArray(value) || value.length === 0) {
                throw new HttpException(400, msg);
            }
            for (const element of value) {
                if (
                    !Array.isArray(element) ||
                    element.length !== 2 ||
                    element.some((val) => typeof val !== "number")
                ) {
                    throw new HttpException(400, msg);
                }
            }
            return true;
        };
    }

    arrayOfNumberTypeTuples(err_msg?: string) {
        return function (value: string, field: string) {
            const msg = err_msg
                ? err_msg
                : `${field}:${value} is not array of tuples [ number, account_type ] with at least one element`;
            if (!Array.isArray(value) || value.length === 0) {
                throw new HttpException(400, msg);
            }
            for (const element of value) {
                if (
                    !Array.isArray(element) ||
                    element.length !== 2 ||
                    !(typeof element[0] === "number") ||
                    !(typeof element[1] === "string")
                ) {
                    throw new HttpException(400, msg);
                }
                if (
                    !(
                        element[1] === config_data.accounts_types.individual ||
                        element[1] === config_data.accounts_types.business ||
                        element[1] === config_data.accounts_types.family
                    )
                ) {
                    throw new HttpException(400, msg);
                }
            }
            return true;
        };
    }
}

const validation_schema = new ValidationSchema();
export default validation_schema;

export function shape(model: ISchema) {
    return (req: Request, res: Response, next: NextFunction): void => {
        const model_keys = Object.keys(model as {});
        const body_keys = Object.keys(req.body as {});
        const reducer = (previousValue: boolean, currentValue: string) =>
            previousValue && model_keys.includes(currentValue);
        const checker = body_keys.reduce(reducer, true);
        if (!checker) {
            throw new HttpException(400, "invalid fields in body");
        }

        const body: IPayload = req.body;
        for (const field in model) {
            const validations = model[field];
            for (const func of validations) {
                body[field] = body[field] || "";
                func(body[field], field);
            }
        }
        next();
    };
}
