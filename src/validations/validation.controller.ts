import action_validator from "./validation.service.js";
import { IAccount, IBusiness, IIndividual } from "../types/types.js";
import config_data from "../config.data.js";
class ValidationController {
    transferValidations(
        src_account: IAccount | IBusiness,
        src_given_type: string,
        amount: number,
        dest_account: IAccount | IBusiness,
        dest_given_type: string,
        limit_flag: boolean,
        fx_flag: boolean
    ) {
        action_validator.accountExist(src_account, src_account.account_id);
        action_validator.accountExist(dest_account, dest_account.account_id);
        action_validator.validateActivation(
            src_account.is_active,
            true,
            src_account.account_id
        );
        action_validator.validateActivation(
            dest_account.is_active,
            true,
            dest_account.account_id
        );
        action_validator.validateType(
            src_account.type,
            src_given_type,
            src_account.account_id
        );
        action_validator.validateType(
            dest_account.type,
            dest_given_type,
            dest_account.account_id
        );
        action_validator.validateCurrency(
            src_account.currency,
            dest_account.currency,
            fx_flag
        );
        action_validator.validateNonNegativeAmount(
            amount,
            src_account.account_id
        );
        action_validator.validateBalanceForTransfer(
            src_account.balance,
            amount,
            src_given_type,
            src_account.account_id
        );

        if (limit_flag) {
            if (
                src_given_type === config_data.accounts_types.business &&
                dest_given_type === config_data.accounts_types.business &&
                action_validator.validatePartners(
                    (src_account as IBusiness).company_id,
                    (dest_account as IBusiness).company_id
                )
            ) {
                action_validator.validateAmountLimits(
                    amount,
                    config_data.accounts_types.partners,
                    src_account.account_id
                );
            } else {
                action_validator.validateAmountLimits(
                    amount,
                    src_given_type,
                    src_account.account_id
                );
            }
        }

        return true;
    }

    individualToFamilyValidations(
        src_account: IIndividual,
        amount: number,
        family_currency: string
    ) {
        action_validator.accountExist(src_account, src_account.account_id);
        action_validator.validateActivation(
            src_account.is_active,
            true,
            src_account.account_id
        );
        action_validator.validateType(
            src_account.type,
            config_data.accounts_types.individual,
            src_account.account_id
        );
        action_validator.validateCurrency(
            src_account.currency,
            family_currency,
            false
        );
        action_validator.validateNonNegativeAmount(
            amount,
            src_account.account_id
        );
        action_validator.validateBalanceForTransfer(
            src_account.balance,
            amount,
            config_data.accounts_types.individual,
            src_account.account_id
        );
        return true;
    }

    changeStatusValidations(status: boolean, account: IAccount) {
        action_validator.accountExist(account, account.account_id);
        action_validator.isNotFamilyAccount(account.type, account.account_id);
        action_validator.validateActivation(
            account.is_active,
            !status,
            account.account_id
        );
    }
}
const validation_controller = new ValidationController();
export default validation_controller;
