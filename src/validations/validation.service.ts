/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import config_data from "../config.data.js";
import HttpException from "../exceptions/http.exception.js";
import { accounts, IAccount, accountsTypes } from "../types/types.js";
class ValidationService {
    accountExist(account: IAccount, account_id: number): boolean {
        if (account) return true;
        throw new HttpException(400, `account ${account_id} does not exist`);
    }

    validateActivation(
        account_status: boolean,
        desired_status: boolean,
        account_id: number
    ): boolean {
        if (account_status == desired_status) return true;
        const status = desired_status
            ? config_data.account_status.active
            : config_data.account_status.deactive;
        throw new HttpException(400, `account ${account_id} is not ${status}`);
    }

    validateType(
        account_type: string,
        given_type: string,
        account_id: number
    ): boolean {
        if (account_type === given_type) return true;
        throw new HttpException(400, `account ${account_id} has invalid type`);
    }

    validateCurrency(
        account_currency: string,
        given_currency: string,
        fx_flag: boolean
    ): boolean {
        if (fx_flag) {
            return true;
        } else {
            if (account_currency === given_currency) return true;
            throw new HttpException(400, "currencies does not match");
        }
    }

    validateNonNegativeAmount(amount: number, account_id: number): boolean {
        if (amount < 0) {
            throw new HttpException(
                400,
                `account ${account_id} has a negative amount`
            );
        }
        return true;
    }

    validateBalanceForTransfer(
        balance: number,
        amount: number,
        type: string,
        account_id: number
    ): boolean {
        if (
            balance - amount >=
            config_data.minimum_remaining_balance[type as accountsTypes]
        )
            return true;
        throw new HttpException(
            400,
            `account ${account_id} is not allowed to transfer`
        );
    }

    validateAmountLimits(
        amount: number,
        type: string,
        account_id: number
    ): boolean {
        if (
            type === config_data.accounts_types.individual ||
            amount <= config_data.transfer_amount_limit[type as accounts]
        )
            return true;
        throw new HttpException(
            400,
            `account ${account_id} exceeded the transfer amount limit`
        );
    }

    validatePartners(src_company_id: number, dst_company_id: number): boolean {
        if (src_company_id === dst_company_id) return true;
        return false;
    }

    validateInitialAmount(
        initial_amount: number,
        account_type: string
    ): boolean {
        if (
            initial_amount >=
            config_data.minimum_initial_amount[account_type as accountsTypes]
        )
            return true;
        return false;
    }

    isNotFamilyAccount(account_type: string, account_id: number) {
        if (account_type === config_data.accounts_types.family) {
            throw new HttpException(
                400,
                `account ${account_id} is of type family`
            );
        }
        return true;
    }
}

const validation_service = new ValidationService();
export default validation_service;
