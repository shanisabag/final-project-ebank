import HttpException from "../exceptions/http.exception.js";
import { IAuth } from "../types/types.js";
import auth_db_provider from "./auth.db.provider.js";

class AuthService {
    async getSecretKey(access_key: string) {
        const result = await auth_db_provider.getSecretKey(access_key);
        if (!result) {
            throw new HttpException(401, "Unauthorized");
        }
        return (result as Partial<IAuth>).secret_key;
    }

    async getRoles(access_key: string) {
        const result = await auth_db_provider.getRoles(access_key);
        if (!result) {
            return "";
        }
        return (result as Partial<IAuth>).roles;
    }
}

const auth_service = new AuthService();
export default auth_service;
