import { RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";

class AuthDBProvider {
    async getSecretKey(access_key: string) {
        const sql_select = `SELECT secret_key FROM auth WHERE auth.access_key = "${access_key}"`;
        const [result] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return result[0] as Promise<string>;
    }

    async getRoles(access_key: string) {
        const sql_select = `SELECT roles FROM auth WHERE auth.access_key = "${access_key}"`;
        const [result] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return result[0] as Promise<string>;
    }
}

const auth_db_provider = new AuthDBProvider();
export default auth_db_provider;
