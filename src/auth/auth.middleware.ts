/* eslint-disable @typescript-eslint/restrict-template-expressions */
import CryptoJS from "crypto-js";
import { NextFunction, Request, Response } from "express";
import config_data from "../config.data.js";
import HttpException from "../exceptions/http.exception.js";
import auth_service from "./auth.service.js";

export const vertifyAuth = async (
    req: Request,
    res: Response,
    next: NextFunction
): Promise<void> => {
    const req_signature = req.headers["signature"];
    const http_method = req.method.toLowerCase();
    const time_stamp = req.headers["timestamp"];
    const access_key = req.headers["access_key"];
    const salt = req.headers["salt"];
    let fullUrl = `${req.protocol}://${req.get("host")}${req.originalUrl}`;
    const body = req.body;
    const secret_key = await auth_service.getSecretKey(access_key as string); // get secret key from db

    const to_sign = `${http_method}${fullUrl}${salt as string}${time_stamp as string
        }${access_key as string}${secret_key as string}${body}`;
    let signature = CryptoJS.enc.Hex.stringify(
        CryptoJS.HmacSHA256(to_sign, secret_key as string)
    );
    signature = CryptoJS.enc.Base64.stringify(
        CryptoJS.enc.Utf8.parse(signature)
    );

    if (signature !== req_signature) {
        throw new HttpException(401, "Unauthorized");
    }
    next();
};

export const setRoles = async (
    req: Request,
    res: Response,
    next: NextFunction
): Promise<void> => {
    const access_key = req.headers["access_key"];
    const roles = await auth_service.getRoles(access_key as string);
    req.roles = (roles as string).split(",");
    next();
};

export const vertifyAuthorization =
    (roles: string[]) =>
        (req: Request, res: Response, next: NextFunction): void => {
            const is_authorized = req.roles.some((role) => roles.includes(role));
            if (!is_authorized) {
                throw new HttpException(401, "Unauthorized");
            }
            next();
        };

export const vertifySanctionsList = (
    req: Request,
    res: Response,
    next: NextFunction
): void => {
    const access_key = req.headers["access_key"];
    if (config_data.sanctions_list.includes(access_key as string)) {
        throw new HttpException(401, "Unauthorized");
    }
    next();
};
