import log from "@ajar/marker";
import mysql from "mysql2/promise";

export let connection: mysql.Connection;

const connectDB = async (
    host: string,
    port: number,
    database: string,
    user: string,
    password: string
): Promise<void> => {
    connection = await mysql.createConnection({
        host,
        port,
        database,
        user,
        password,
    });
    await connection.connect();
    log.green(" 🏦 Connected to NSO DB 🏦 ");
};
export default connectDB;
