import { NextFunction, Request, Response } from "express";
import fs from "fs/promises";

// log each request made to the server in a single line to request.log
export const logger =
    (path: string) =>
        async (req: Request, res: Response, next: NextFunction): Promise<void> => {
            const newLog = `${req.method} ${req.url} ${Date.now()} \r\n`;
            await fs.appendFile(path, newLog);
            next();
        };