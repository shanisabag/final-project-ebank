import validation_schema from "../validations/validation.schema.js";

export const transaction_scheme = {
    amount: [
        validation_schema.require(),
        validation_schema.number(),
        validation_schema.min(1),
    ],
};
