import { Request, Response } from "express";
import config_data from "../config.data.js";
import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";
import family_service from "../family/family.service.js";
import transaction_service from "./transaction.service.js";

class TransactionController {
    async getAllTransactions(req: Request, res: Response) {
        const transactions = await transaction_service.getAllTransactions();
        res.status(200).json(transactions);
    }

    async getTransactionsByAccountID(req: Request, res: Response) {
        const transactions =
            await transaction_service.getTransactionsByAccountID(
                Number(req.params.account_id)
            );
        res.status(200).json(transactions);
    }

    async createB2BTransfer(req: Request, res: Response): Promise<void> {
        const { amount } = req.body;
        const b2b_transfer = await transaction_service.createTransfer(
            Number(req.params.business_src_id),
            config_data.accounts_types.business,
            Number(req.params.business_dest_id),
            config_data.accounts_types.business,
            amount as number,
            config_data.limit_flag.B2B
        );
        if (!b2b_transfer) {
            throw new UrlNotFoundException(`/api/transfer/${req.path}`);
        }
        res.status(200).json(b2b_transfer);
    }

    async createB2ITransfer(req: Request, res: Response): Promise<void> {
        const { amount } = req.body;
        const b2i_transfer = await transaction_service.createTransfer(
            Number(req.params.business_src_id),
            config_data.accounts_types.business,
            Number(req.params.individual_dest_id),
            config_data.accounts_types.individual,
            amount as number,
            config_data.limit_flag.B2I
        );
        if (!b2i_transfer) {
            throw new UrlNotFoundException(`/api/transfer/${req.path}`);
        }
        res.status(200).json(b2i_transfer);
    }

    async createF2BTransfer(req: Request, res: Response): Promise<void> {
        const { amount } = req.body;
        const f2b_transfer = await transaction_service.createTransfer(
            Number(req.params.family_src_id),
            config_data.accounts_types.family,
            Number(req.params.business_dest_id),
            config_data.accounts_types.business,
            amount as number,
            config_data.limit_flag.F2B
        );
        if (!f2b_transfer) {
            throw new UrlNotFoundException(`/api/transfer/${req.path}`);
        }
        res.status(200).json(f2b_transfer);
    }

    async createB2BFXTransfer(req: Request, res: Response): Promise<void> {
        const { amount } = req.body;
        const b2b_fx_transfer = await transaction_service.createTransfer(
            Number(req.params.business_src_id),
            config_data.accounts_types.business,
            Number(req.params.business_dest_id),
            config_data.accounts_types.business,
            amount as number,
            config_data.limit_flag.B2BFX,
            true
        );
        if (!b2b_fx_transfer) {
            throw new UrlNotFoundException(`/api/transfer/${req.path}`);
        }
        res.status(200).json(b2b_fx_transfer);
    }

    async createI2FTransfer(req: Request, res: Response): Promise<void> {
        const individual_account_id = Number(req.params.individual_src_id);
        const family_account_id = Number(req.params.family_dest_id);
        const { amount } = req.body;

        // check if individual belong to family
        await family_service.isBelongToFamily(
            individual_account_id,
            family_account_id
        );

        const i2f_transfer = await transaction_service.createTransfer(
            individual_account_id,
            config_data.accounts_types.individual,
            family_account_id,
            config_data.accounts_types.family,
            amount as number,
            config_data.limit_flag.I2F
        );

        if (!i2f_transfer) {
            throw new UrlNotFoundException(`/api/transfer/${req.path}`);
        }
        res.status(200).json(i2f_transfer);
    }
}

const transaction_controller = new TransactionController();
export default transaction_controller;
