/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
import { vertifySanctionsList } from "../auth/auth.middleware.js";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import transaction_controller from "./transaction.controller.js";
import { transaction_scheme } from "./transaction.schema.js";

const router = express.Router();

// verify sanctions list
router.use(raw(vertifySanctionsList));

// get all transactions
router.get("/", raw(transaction_controller.getAllTransactions));

// get account's transactions by account id
router.get(
    "/:account_id",
    raw(transaction_controller.getTransactionsByAccountID)
);

// transfer from a business account to a business account having the same currency
router.post(
    "/:business_src_id/trasfer_b2b/:business_dest_id",
    shape(transaction_scheme),
    raw(transaction_controller.createB2BTransfer)
);

// transfer from a business account to an individual account having the same currency
router.post(
    "/:business_src_id/trasfer_b2i/:individual_dest_id",
    shape(transaction_scheme),
    raw(transaction_controller.createB2ITransfer)
);

// transfer from a family account to a business account having the same currency
router.post(
    "/:family_src_id/trasfer_f2b/:business_dest_id",
    shape(transaction_scheme),
    raw(transaction_controller.createF2BTransfer)
);

// transfer from a business account to a business account with FX (accounts have a different currency)
router.post(
    "/:business_src_id/trasfer_FX_b2b/:business_dest_id",
    shape(transaction_scheme),
    raw(transaction_controller.createB2BFXTransfer)
);

// transfer from a individual account to a family account
router.post(
    "/:individual_src_id/trasfer_I2F/:family_dest_id",
    shape(transaction_scheme),
    raw(transaction_controller.createI2FTransfer)
);

export default router;
