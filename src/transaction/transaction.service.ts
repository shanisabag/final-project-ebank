import account_db_provider from "../account/account.db.provider.js";
import transaction_db_provider from "./transaction.db.provider.js";
import trasaction_utils from "../utils/transaction.utils.js";
import validation_controller from "../validations/validation.controller.js";
import business_service from "../business/business.service.js";
import account_service from "../account/account.service.js";
import HttpException from "../exceptions/http.exception.js";
import config_data from "../config.data.js";
import { TransactionResult } from "../types/result.types.js";

class TransactionService {
    async getAllTransactions() {
        const transactions = await transaction_db_provider.getAllTransactions();
        if (transactions.length === 0) {
            throw new HttpException(204, "no transactions have been made yet");
        }
        return transactions;
    }

    async getTransactionsByAccountID(account_id: number) {
        const transactions =
            await transaction_db_provider.getTransactionsByAccountID(
                account_id
            );
        if (transactions.length === 0) {
            throw new HttpException(400, "no transactions have been made yet");
        }
        return transactions;
    }

    async createTransfer(
        src_account_id: number,
        src_type: string,
        dest_account_id: number,
        dest_type: string,
        transaction_amount: number,
        limit_flag: boolean,
        fx: boolean = false
    ): Promise<TransactionResult> {
        let rate = 1;
        let src_account;
        let dest_account;
        if (
            src_type === config_data.accounts_types.business &&
            dest_type === config_data.accounts_types.business
        ) {
            src_account = await business_service.getBusinessAccount(
                src_account_id
            );
            dest_account = await business_service.getBusinessAccount(
                dest_account_id
            );
        } else {
            src_account = await account_service.getAccount(src_account_id);
            dest_account = await account_service.getAccount(dest_account_id);
        }

        // validate transaction
        validation_controller.transferValidations(
            src_account,
            src_type,
            transaction_amount,
            dest_account,
            dest_type,
            limit_flag,
            fx
        );

        if (fx) {
            rate = await trasaction_utils.getRate(
                src_account.currency,
                dest_account.currency
            );
        }

        const src_balance_after_trs =
            src_account.balance - transaction_amount / rate;
        const dest_amount_after_trs = dest_account.balance + transaction_amount;

        const result_header = await account_db_provider.updateTransferBalances(
            src_account_id,
            dest_account_id,
            src_balance_after_trs,
            dest_amount_after_trs
        );

        const transaction_status = result_header ? "completed" : "failed";
        await transaction_db_provider.createTransaction(
            src_account_id,
            dest_account_id,
            transaction_amount,
            dest_account.currency,
            transaction_status
        );

        const result_obj = trasaction_utils.createTransactionObjectResult(
            src_account_id,
            src_balance_after_trs,
            src_account.currency,
            dest_account_id,
            dest_amount_after_trs,
            dest_account.currency
        );

        // log transaction to transactions file
        await trasaction_utils.transactionsLogs(
            config_data.transaction_log_file_path,
            result_obj,
            transaction_amount,
            transaction_status
        );

        return result_obj;
    }
}

const transaction_service = new TransactionService();
export default transaction_service;
