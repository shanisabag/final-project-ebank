import fs from "fs";
import path, { dirname } from "path";
import { fileURLToPath } from "url";
import {
    IAccountStatus,
    IAccountsTypes,
    ILimitFlag,
    IMinimumInitialAmount,
    IMinimumRemainingBalance,
    IRoles,
    ISqlData,
    ITransferAmountLimit,
} from "./types/config.types.js";

class ConfigData {
    host: string;
    port: number;
    sql: ISqlData;
    exchangeratesapi_key: string;
    log_file_path: string;
    error_log_file_path: string;
    transaction_log_file_path: string;
    accounts_types: IAccountsTypes;
    account_status: IAccountStatus;
    minimum_initial_amount: IMinimumInitialAmount;
    minimum_remaining_balance: IMinimumRemainingBalance;
    transfer_amount_limit: ITransferAmountLimit;
    sanctions_list: string[];
    supported_currencies: string[];
    roles: IRoles;
    limit_flag: ILimitFlag;
    get_rate_access_key: string;

    constructor() {
        const __dirname = dirname(fileURLToPath(import.meta.url));
        const config_path = path.resolve(__dirname, "../config.json");
        const config_data = fs.readFileSync(config_path, {
            encoding: "utf8",
            flag: "r",
        });
        const {
            host,
            port,
            sql,
            exchangeratesapi_key,
            log_file_path,
            error_log_file_path,
            transaction_log_file_path,
            accounts_types,
            account_status,
            minimum_initial_amount,
            minimum_remaining_balance,
            transfer_amount_limit,
            sanctions_list,
            supported_currencies,
            roles,
            limit_flag,
            get_rate_access_key
        } = JSON.parse(config_data);
        this.host = host;
        this.port = port;
        this.sql = sql;
        this.exchangeratesapi_key = exchangeratesapi_key;
        this.log_file_path = log_file_path;
        this.error_log_file_path = error_log_file_path;
        this.transaction_log_file_path = transaction_log_file_path;
        this.accounts_types = accounts_types;
        this.account_status = account_status;
        this.minimum_initial_amount = minimum_initial_amount;
        this.minimum_remaining_balance = minimum_remaining_balance;
        this.transfer_amount_limit = transfer_amount_limit;
        this.sanctions_list = sanctions_list;
        this.supported_currencies = supported_currencies;
        this.roles = roles;
        this.limit_flag = limit_flag;
        this.get_rate_access_key = get_rate_access_key;

    }
}

const config_data = new ConfigData();
export default config_data;
