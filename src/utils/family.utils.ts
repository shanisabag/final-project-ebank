import { RowDataPacket } from "mysql2";
import {
    ShortFamilyResult,
    FullFamilyResult,
    IFamilyResultRowDataPacket,
} from "../types/result.types.js";

export function createShortFamilyResult(
    sql_result: RowDataPacket
): ShortFamilyResult {
    const result: IFamilyResultRowDataPacket = sql_result[0];
    const resultObj: ShortFamilyResult = {
        account_id: result.family_account_id,
        currency: result.family_account_currency,
        balance: result.family_account_balance,
        is_active: result.family_account_is_active,
        type: result.family_account_type,
        family_table_id: result.family_table_id,
        context: result.family_table_context,
        owners: [],
    };
    if (result.account_id !== null) {
        (sql_result as IFamilyResultRowDataPacket[]).forEach(
            (account: IFamilyResultRowDataPacket) =>
                resultObj.owners.push(Number(account.account_id))
        );
    }
    return resultObj;
}

export function createFullFamilyResult(
    sql_result: RowDataPacket
): FullFamilyResult {
    const result: IFamilyResultRowDataPacket = sql_result[0];
    const resultObj: FullFamilyResult = {
        account_id: result.family_account_id,
        currency: result.family_account_currency,
        balance: result.family_account_balance,
        is_active: result.family_account_is_active,
        type: result.family_account_type,
        family_table_id: result.family_table_id,
        context: result.family_table_context,
        owners: [],
    };
    if (result.account_id !== null) {
        (sql_result as IFamilyResultRowDataPacket[]).forEach(
            (account: IFamilyResultRowDataPacket) => {
                let individual_account = {
                    account_id: account.account_id,
                    currency: account.currency,
                    balance: account.balance,
                    is_active: account.is_active,
                    type: account.type,
                    individual_table_id: account.individual_table_id,
                    individual_id: account.individual_id,
                    first_name: account.first_name,
                    last_name: account.last_name,
                    email: account.email,
                    address: {
                        address_id: account.address_id,
                        country_name: account.country_name,
                        country_code: account.country_code,
                        postal_code: account.postal_code,
                        city: account.city,
                        region: account.region,
                        street_name: account.street_name,
                        street_number: account.street_number,
                    },
                };
                resultObj.owners.push(individual_account);
            }
        );
    }
    return resultObj;
}
