import { Request, Response } from "express";
import { IBusiness } from "../types/types.js";
import business_service from "./business.service.js";
class BusinessController {
    async getBusinessAccount(req: Request, res: Response): Promise<void> {
        const business_account = await business_service.getBusinessAccount(
            Number(req.params.account_id)
        );
        res.status(200).json(business_account);
    }

    async createBusinessAccount(req: Request, res: Response): Promise<void> {
        const account_id = await business_service.createBusinessAccount(
            req.body as Partial<IBusiness>
        );
        const business_account = await business_service.getBusinessAccount(
            account_id
        );
        res.status(200).json(business_account);
    }
}

const business_controller = new BusinessController();
export default business_controller;
