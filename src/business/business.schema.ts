import config_data from "../config.data.js";
import validation_schema from "../validations/validation.schema.js";

export const business_schema = {
    company_id: [
        validation_schema.require(),
        validation_schema.number(),
        validation_schema.min(10000000),
        validation_schema.maxLength(
            8,
            "company_id should be a number with 8 digits"
        ),
    ],
    company_name: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.minLength(2),
        validation_schema.maxLength(40),
    ],
    context: [validation_schema.string()],
    address: [],
    currency: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.currencies(config_data.supported_currencies),
    ],
    balance: [validation_schema.number()],
};
