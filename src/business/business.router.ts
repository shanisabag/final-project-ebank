/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
import { vertifySanctionsList } from "../auth/auth.middleware.js";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import business_controller from "./business.controller.js";
import { business_schema } from "./business.schema.js";

const router = express.Router();

// get a business account
router.get("/:account_id", raw(business_controller.getBusinessAccount));

// verify sanctions list
router.use(raw(vertifySanctionsList));

// create a business account
router.post(
    "/",
    shape(business_schema),
    raw(business_controller.createBusinessAccount)
);

export default router;
