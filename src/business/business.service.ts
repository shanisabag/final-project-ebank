import { IBusiness } from "../types/types.js";
import business_db_provider from "./business.db.provider.js";
import HttpException from "../exceptions/http.exception.js";
import address_service from "../address/address.service.js";
import account_service from "../account/account.service.js";
import config_data from "../config.data.js";
class BusinessService {
    async getBusinessAccount(account_id: number): Promise<IBusiness> {
        const account = await business_db_provider.getBusinessAccount(
            account_id
        );
        if (!account) {
            throw new HttpException(
                400,
                `account ${account_id} does not exsit`
            );
        }
        return account;
    }

    async createBusinessAccount(payload: Partial<IBusiness>): Promise<number> {
        let address_id;
        const {
            currency,
            balance,
            address,
            company_id,
            company_name,
            context,
        } = payload;

        if (address) {
            address_id = await address_service.createAddress(address);
        }

        const account_id = await account_service.createAccount(
            currency as string,
            Number(balance),
            config_data.accounts_types.business
        );
        await business_db_provider.createBusinessAccount(
            Number(company_id),
            company_name as string,
            context as string,
            account_id,
            address_id as number
        );

        return account_id;
    }
}

const business_service = new BusinessService();
export default business_service;
