import { ResultSetHeader, RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";
import { IBusiness } from "../types/types.js";
class BusinessDBProvider {
    async getBusinessAccount(account_id: number) {
        const sql_select = `SELECT account.*, business_account.* FROM account INNER JOIN business_account ON account.account_id = business_account.account_id WHERE account.account_id = ${account_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return account[0] as Promise<IBusiness>;
    }

    async createBusinessAccount(
        company_id: number,
        company_name: string,
        context: string,
        account_id: number,
        address_id: number
    ) {
        const sql_insert = `INSERT INTO business_account (company_id, company_name, context, account_id, address_id) VALUES (${company_id}, "${company_name}", "${context}", ${account_id}, ${address_id})`;
        const [account] = (await connection.query(
            sql_insert
        )) as ResultSetHeader[];
        return account.insertId;
    }
}

const business_db_provider = new BusinessDBProvider();
export default business_db_provider;
