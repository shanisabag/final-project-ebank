import { ResultSetHeader, RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";
import { IAccount } from "../types/types.js";
class AccountlDBProvider {
    async getAccount(account_id: number) {
        const sql_select = `SELECT * FROM account WHERE account.account_id = ${account_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return account[0] as Promise<IAccount>;
    }

    async getAccountWithType(account_id: number, type: string) {
        const sql_select = `SELECT * FROM account WHERE account.account_id = ${account_id} AND account.type = "${type}"`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return account[0] as Promise<IAccount>;
    }

    async createAccount(
        currency: string,
        balance: number,
        type: string
    ): Promise<number> {
        const sql_insert = `INSERT INTO account (currency, balance, is_active, type) VALUES ("${currency}", ${balance}, 1, "${type}")`;
        const [account] = (await connection.query(
            sql_insert
        )) as ResultSetHeader[];
        return account.insertId;
    }

    async updateTransferBalances(
        src_account_id: number,
        dest_account_id: number,
        src_balance: number,
        dest_balance: number
    ): Promise<ResultSetHeader> {
        const sql_update = `UPDATE account SET balance = CASE WHEN account_id = ? THEN ?
                            WHEN account_id = ? THEN ? 
                            END
                            WHERE account_id = ? OR account_id = ?`;
        const [result_header] = (await connection.query(sql_update, [
            src_account_id,
            src_balance,
            dest_account_id,
            dest_balance,
            src_account_id,
            dest_account_id,
        ])) as ResultSetHeader[];
        return result_header;
    }

    async updateAccountActivation(account_id: number, is_active: boolean) {
        const sql_update = `UPDATE account SET ? WHERE account_id = ${account_id}`;
        const [result_header] = (await connection.query(sql_update, {
            is_active: is_active,
        })) as ResultSetHeader[];
        return result_header;
    }
}

const account_db_provider = new AccountlDBProvider();
export default account_db_provider;
