/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import account_controller from "./account.controller.js";
import { change_status_schema } from "./account.schema.js";
import {
    vertifyAuthorization,
    vertifySanctionsList,
} from "../auth/auth.middleware.js";
import config_data from "../config.data.js";

const router = express.Router();

// verify sanctions list
router.use(raw(vertifySanctionsList));

// change accounts status
router.patch(
    "/status",
    raw(vertifyAuthorization([config_data.roles.admin, config_data.roles.super_user])),
    shape(change_status_schema),
    raw(account_controller.changeStatus)
);

export default router;
