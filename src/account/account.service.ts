import { accountsTypes, IAccount } from "../types/types.js";
import account_db_provider from "./account.db.provider.js";
import HttpException from "../exceptions/http.exception.js";
import validation_controller from "../validations/validation.controller.js";
import { connection } from "../db/mysql.connection.js";
import config_data from "../config.data.js";
import { AccountsStatusResponse } from "../types/result.types.js";

class AccountService {
    async getAccount(account_id: number): Promise<IAccount> {
        const account = await account_db_provider.getAccount(account_id);
        if (!account) {
            throw new HttpException(
                400,
                `account ${account_id} does not exsit`
            );
        }
        return account;
    }

    async getAccountWithType(account_id: number, type: string): Promise<IAccount> {
        const account = await account_db_provider.getAccountWithType(account_id, type);
        if (!account) {
            throw new HttpException(
                400,
                `account ${account_id} does not exsit`
            );
        }
        return account;
    }

    async createAccount(
        currency: string,
        balance: number,
        type: string
    ): Promise<number> {
        const account_id = await account_db_provider.createAccount(
            currency,
            balance,
            type
        );
        return account_id;
    }

    async changeStatus(
        action: string,
        accounts: [number, accountsTypes][]
    ): Promise<AccountsStatusResponse> {
        const action_bool =
            action === config_data.account_status.active ? true : false;
        for (const [account_id, account_type] of accounts) {
            const account = await this.getAccountWithType(account_id, account_type);
            validation_controller.changeStatusValidations(action_bool, account);
        }

        await connection.beginTransaction();
        try {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            for (const [account_id, account_type] of accounts) {
                await account_db_provider.updateAccountActivation(
                    account_id,
                    action_bool
                );
            }
            await connection.commit();
        } catch (error) {
            await connection.rollback();
            throw error;
        }

        const response = {
            accounts: accounts,
            status: action,
        };
        return response;
    }
}

const account_service = new AccountService();
export default account_service;
