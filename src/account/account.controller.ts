import { Request, Response } from "express";
import HttpException from "../exceptions/http.exception.js";
import { accountsTypes } from "../types/types.js";
import accounts_service from "./account.service.js";

class AccountController {
    async changeStatus(req: Request, res: Response): Promise<void> {
        const { owners, action } = req.body;
        const response = await accounts_service.changeStatus(
            action as string,
            owners as [number, accountsTypes][]
        );
        if (!response) {
            throw new HttpException(400, `can not complete the action`);
        }
        res.status(200).json(response);
    }
}
const account_controller = new AccountController();
export default account_controller;
