import config_data from "../config.data.js";
import validation_schema from "../validations/validation.schema.js";

export const change_status_schema = {
    owners: [
        validation_schema.require(),
        validation_schema.arrayOfNumberTypeTuples(),
    ],
    action: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.oneOf([
            config_data.account_status.active,
            config_data.account_status.deactive,
        ]),
    ],
};
