import { ResultSetHeader, RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";
import { IIndividual } from "../types/types.js";

class IndividualDBProvider {
    async getIndividualAccount(account_id: number) {
        const sql_select = `SELECT account.*, individual_account.* FROM account INNER JOIN individual_account ON account.account_id = individual_account.account_id WHERE account.account_id = ${account_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return account[0] as Promise<IIndividual>;
    }

    async getIndividualById(individual_id: number) {
        const sql_select = `SELECT individual_account.* FROM individual_account WHERE individual_account.individual_id = ${individual_id}`;
        const [account] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return account[0] as Promise<IIndividual>;
    }

    async createIndividualAccount(
        individual_id: number,
        first_name: string,
        last_name: string,
        email: string,
        account_id: number,
        address_id: number
    ) {
        const sql_insert = `INSERT INTO individual_account (individual_id, first_name, last_name, email, account_id, address_id) VALUES (${individual_id}, "${first_name}", "${last_name}", "${email}", ${account_id}, ${address_id})`;
        const [account] = (await connection.query(
            sql_insert
        )) as ResultSetHeader[];
        return account.insertId;
    }
}

const individual_db_provider = new IndividualDBProvider();
export default individual_db_provider;
