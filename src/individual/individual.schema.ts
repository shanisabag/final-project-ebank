import config_data from "../config.data.js";
import validation_schema from "../validations/validation.schema.js";

export const individual_scheme = {
    individual_id: [
        validation_schema.require(),
        validation_schema.number(),
        validation_schema.min(1000000),
        validation_schema.maxLength(
            7,
            "individual_id should be a number with 7 digits"
        ),
    ],
    first_name: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.minLength(2),
        validation_schema.maxLength(20),
    ],
    last_name: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.minLength(2),
        validation_schema.maxLength(20),
    ],
    email: [validation_schema.string(), validation_schema.mail()],
    address: [],
    currency: [
        validation_schema.require(),
        validation_schema.string(),
        validation_schema.currencies(config_data.supported_currencies),
    ],
    balance: [validation_schema.number()],
};
