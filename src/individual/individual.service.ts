import { IIndividual } from "../types/types.js";
import individual_db_provider from "./individual.db.provider.js";
import HttpException from "../exceptions/http.exception.js";
import address_service from "../address/address.service.js";
import account_service from "../account/account.service.js";
import config_data from "../config.data.js";
class IndividualService {
    async getIndividualAccount(account_id: number): Promise<IIndividual> {
        const account = await individual_db_provider.getIndividualAccount(
            account_id
        );
        if (!account) {
            throw new HttpException(
                400,
                `account ${account_id} does not exsit.`
            );
        }
        return account;
    }

    async createIndividualAccount(
        payload: Partial<IIndividual>
    ): Promise<number> {
        let address_id;

        const {
            currency,
            balance,
            address,
            individual_id,
            first_name,
            last_name,
            email,
        } = payload;

        // check if individual account already exists (by individual_id)
        const account_exist = await individual_db_provider.getIndividualById(
            individual_id as number
        );
        if (account_exist) {
            throw new HttpException(400, "individual account already exists");
        }

        if (address) {
            address_id = await address_service.createAddress(address);
        }

        const account_id = await account_service.createAccount(
            currency as string,
            Number(balance),
            config_data.accounts_types.individual
        );

        await individual_db_provider.createIndividualAccount(
            Number(individual_id),
            first_name as string,
            last_name as string,
            email as string,
            account_id,
            address_id as number
        );

        return account_id;
    }
}

const individual_service = new IndividualService();
export default individual_service;
