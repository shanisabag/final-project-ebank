/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
import { vertifySanctionsList } from "../auth/auth.middleware.js";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import individual_controller from "./individual.controller.js";
import { individual_scheme } from "./individual.schema.js";

const router = express.Router();

// get an individual account
router.get("/:account_id", raw(individual_controller.getIndividualAccount));

// verify sanctions list
router.use(raw(vertifySanctionsList));

// create an individual account
router.post(
    "/",
    shape(individual_scheme),
    raw(individual_controller.createIndividualAccount)
);

export default router;
