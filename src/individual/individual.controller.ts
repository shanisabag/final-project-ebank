import { Request, Response } from "express";
import { IIndividual } from "../types/types.js";
import individual_service from "./individual.service.js";

class IndividualController {
    async getIndividualAccount(req: Request, res: Response): Promise<void> {
        const individual_account =
            await individual_service.getIndividualAccount(
                Number(req.params.account_id)
            );
        res.status(200).json(individual_account);
    }

    async createIndividualAccount(req: Request, res: Response): Promise<void> {
        const account_id = await individual_service.createIndividualAccount(
            req.body as Partial<IIndividual>
        );
        const individual_account =
            await individual_service.getIndividualAccount(account_id);
        res.status(200).json(individual_account);
    }
}

const individual_controller = new IndividualController();
export default individual_controller;
