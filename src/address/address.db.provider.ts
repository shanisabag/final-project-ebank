import { ResultSetHeader } from "mysql2";
import { connection } from "../db/mysql.connection.js";
import { IAddress } from "../types/types.js";

class AddresslDBProvider {
    async createAddress(payload: IAddress): Promise<number> {
        const sql_insert = "INSERT INTO address SET ?";
        const [address] = (await connection.query(
            sql_insert,
            payload
        )) as ResultSetHeader[];
        return address.insertId;
    }
}

const address_db_provider = new AddresslDBProvider();
export default address_db_provider;
