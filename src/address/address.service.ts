import { IAddress } from "../types/types.js";
import address_db_provider from "./address.db.provider.js";

class AddressService {
    async createAddress(payload: IAddress): Promise<number> {
        const address_id = await address_db_provider.createAddress(payload);
        return address_id;
    }
}

const address_service = new AddressService();
export default address_service;
